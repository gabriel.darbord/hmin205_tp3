package com.example.hmin205_tp3_exo1;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class PlanningActivity extends AppCompatActivity {
    private static final String filename = "planning";
    private TextView tvTimeSlot1;
    private TextView tvTimeSlot2;
    private TextView tvTimeSlot3;
    private TextView tvTimeSlot4;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planning);

        tvTimeSlot1 = findViewById(R.id.time_slot_1);
        tvTimeSlot2 = findViewById(R.id.time_slot_2);
        tvTimeSlot3 = findViewById(R.id.time_slot_3);
        tvTimeSlot4 = findViewById(R.id.time_slot_4);

        // utilisation de LiveData pour rafraîchir le planning
        PlanningModel model = new ViewModelProvider(this).get(PlanningModel.class);
        model.getTimeSlot1().observe(this,
                timeSlot -> tvTimeSlot1.setText(getString(R.string.time_slot_1, timeSlot)));
        model.getTimeSlot2().observe(this,
                timeSlot -> tvTimeSlot2.setText(getString(R.string.time_slot_2, timeSlot)));
        model.getTimeSlot3().observe(this,
                timeSlot -> tvTimeSlot3.setText(getString(R.string.time_slot_3, timeSlot)));
        model.getTimeSlot4().observe(this,
                timeSlot -> tvTimeSlot4.setText(getString(R.string.time_slot_4, timeSlot)));

        /* Exercice 5 : sauvegarde dans un fichier
        // création du fichier "planning" s'il n'existe pas encore
        File file = new File(getFilesDir(), "planning");
        if(!file.exists()) {
            String contents = "Rencontre client Dupont\n"
                    + "Travailler le dossier recrutement\n"
                    + "Réunion équipe\n"
                    + "Préparation dossier vente";
            try(FileOutputStream out = openFileOutput(filename, MODE_PRIVATE)) {
                out.write(contents.getBytes());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // mise à jour des informations en lisant le fichier "planning"
        try(FileInputStream in = openFileInput(filename)) {
            InputStreamReader inputStreamReader = new InputStreamReader(in);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            model.getTimeSlot1().setValue(bufferedReader.readLine());
            model.getTimeSlot2().setValue(bufferedReader.readLine());
            model.getTimeSlot3().setValue(bufferedReader.readLine());
            model.getTimeSlot4().setValue(bufferedReader.readLine());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

        /* Exercice 6 : sauvegarde avec utilisation de Room */
        Long userId = getIntent().getLongExtra("userId", 0);
        AppDatabase database = AppDatabase.getInstance(this);
        PlanningDao planningDao = database.PlanningDao();
        // création d'un planning par défaut
        Planning planning = new Planning(userId,
                "Rencontre client Dupont",
                "Travailler le dossier recrutement",
                "Réunion équipe",
                "Préparation dossier vente");
        // pour cet exercice, on veut permettre les allers-retours entre
        // les activités, donc pour éviter les contraintes de clé primaire
        // on va simplement supprimer et réinsérer le planning à chaque fois
        planningDao.deletePlannings(planningDao.getAll());
        planningDao.insertPlanning(planning);


        // on peut récupérer le planning depuis la BDD pour montrer que ça marche
        // cependant il faut avoir fait une inscription pour avoir une entrée
        // dans la table des utilisateurs, or ici on n'a pas de garantie que cela
        // a été fait car cette activité est accessible depuis l'inscription
        //planning = planningDao.findPlanningWithUserId(userId);
        model.update(planning);
    }
}