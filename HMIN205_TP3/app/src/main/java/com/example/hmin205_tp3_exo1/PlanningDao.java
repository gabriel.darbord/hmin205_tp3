package com.example.hmin205_tp3_exo1;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PlanningDao {
    @Insert
    void insertPlanning(Planning planning);

    @Query("SELECT * FROM planning")
    List<Planning> getAll();

    @Query("SELECT * FROM planning INNER JOIN user ON uid = id WHERE id = :userId")
    Planning findPlanningWithUserId(Long userId);

    @Delete
    void deletePlannings(List<Planning> plannings);
}
