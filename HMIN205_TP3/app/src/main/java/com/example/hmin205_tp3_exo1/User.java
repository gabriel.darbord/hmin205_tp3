package com.example.hmin205_tp3_exo1;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class User {
    @PrimaryKey
    public Long id;

    @ColumnInfo(name = "last_name")
    public String lastName;

    @ColumnInfo(name = "first_name")
    public String firstName;

    @ColumnInfo(name = "age")
    public String age;

    @ColumnInfo(name = "phone_number")
    public String phoneNumber;

    public User(Long id, String lastName, String firstName, String age, String phoneNumber) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.age = age;
        this.phoneNumber = phoneNumber;
    }
}
