package com.example.hmin205_tp3_exo1;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity
public class Planning {
    @ForeignKey(entity = User.class, parentColumns = "id", childColumns = "uid")
    @PrimaryKey
    public Long uid;

    @ColumnInfo(name = "timeslot1")
    public String timeSlot1;

    @ColumnInfo(name = "timeslot2")
    public String timeSlot2;

    @ColumnInfo(name = "timeslot3")
    public String timeSlot3;

    @ColumnInfo(name = "timeslot4")
    public String timeSlot4;

    public Planning(Long uid, String timeSlot1, String timeSlot2, String timeSlot3, String timeSlot4) {
        this.uid = uid;
        this.timeSlot1 = timeSlot1;
        this.timeSlot2 = timeSlot2;
        this.timeSlot3 = timeSlot3;
        this.timeSlot4 = timeSlot4;
    }
}
