package com.example.hmin205_tp3_exo1;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PlanningModel extends ViewModel {
    private MutableLiveData<String> timeSlot1;
    private MutableLiveData<String> timeSlot2;
    private MutableLiveData<String> timeSlot3;
    private MutableLiveData<String> timeSlot4;

    public MutableLiveData<String> getTimeSlot1() {
        if(timeSlot1 == null) {
            timeSlot1 = new MutableLiveData<>();
        }
        return timeSlot1;
    }

    public MutableLiveData<String> getTimeSlot2() {
        if(timeSlot2 == null) {
            timeSlot2 = new MutableLiveData<>();
        }
        return timeSlot2;
    }

    public MutableLiveData<String> getTimeSlot3() {
        if(timeSlot3 == null) {
            timeSlot3 = new MutableLiveData<>();
        }
        return timeSlot3;
    }

    public MutableLiveData<String> getTimeSlot4() {
        if(timeSlot4 == null) {
            timeSlot4 = new MutableLiveData<>();
        }
        return timeSlot4;
    }

    public void update(Planning planning) {
        getTimeSlot1().setValue(planning.timeSlot1);
        getTimeSlot2().setValue(planning.timeSlot2);
        getTimeSlot3().setValue(planning.timeSlot3);
        getTimeSlot4().setValue(planning.timeSlot4);
    }
}
