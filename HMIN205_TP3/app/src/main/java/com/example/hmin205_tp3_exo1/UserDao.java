package com.example.hmin205_tp3_exo1;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserDao {
    @Insert
    void insertUser(User user);

    @Query("SELECT * FROM user")
    List<User> getAll();

    @Delete
    void deleteUsers(List<User> users);
}
