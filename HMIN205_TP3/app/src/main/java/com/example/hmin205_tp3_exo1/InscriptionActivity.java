package com.example.hmin205_tp3_exo1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class InscriptionActivity extends AppCompatActivity {
    private AppDatabase database;
    private Long id;
    private EditText etLastName;
    private EditText etFirstName;
    private EditText etAge;
    private EditText etPhoneNumber;
    private TextView tvId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        etLastName = findViewById(R.id.signup_last_name);
        etFirstName = findViewById(R.id.signup_first_name);
        etAge = findViewById(R.id.signup_age);
        etPhoneNumber = findViewById(R.id.signup_phone_number);
        tvId = findViewById(R.id.signup_id);

        if(savedInstanceState == null) {
            generateId();
            tvId.setText(getString(R.string.id, id));
        }

        getLifecycle().addObserver(new Utilisation());

        database = AppDatabase.getInstance(this);
    }


    private void generateId() {
        id = new Random().nextLong();
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("last_name", etLastName.getText().toString());
        outState.putString("first_name", etFirstName.getText().toString());
        outState.putString("age", etAge.getText().toString());
        outState.putString("phone_number", etPhoneNumber.getText().toString());
        outState.putLong("id", id);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        etLastName.setText(savedInstanceState.getString("last_name"));
        etFirstName.setText(savedInstanceState.getString("first_name"));
        etAge.setText(savedInstanceState.getString("age"));
        etPhoneNumber.setText(savedInstanceState.getString("phone_number"));
        id = savedInstanceState.getLong("id");
        tvId.setText(getString(R.string.id, id));
    }


    public void submit(View v) {
        // récupération des valeurs
        String userId = id.toString();
        String lastName = etLastName.getText().toString();
        String firstName = etFirstName.getText().toString();
        String age = etAge.getText().toString();
        String phoneNumber = etPhoneNumber.getText().toString();

        // sauvegarde des infos dans un fichier
        String filename = lastName + userId;
        String contents = lastName + "\n"
                + firstName + "\n"
                + age + "\n"
                + phoneNumber + "\n"
                + userId + "\n"
                + "Nombre d'utilisations : " + Utilisation.COMPTEUR;

        try(FileOutputStream out = openFileOutput(filename, MODE_PRIVATE)) {
            out.write(contents.getBytes());
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // sauvegarde des infos dans la BDD
        User user = new User(id, lastName, firstName, age, phoneNumber);
        UserDao userDao = database.UserDao();
        // pour cet exercice, on veut permettre les allers-retours entre
        // les activités, donc pour éviter les contraintes de clé primaire
        // on va simplement supprimer et réinsérer l'utilisateur à chaque fois
        userDao.deleteUsers(userDao.getAll());
        userDao.insertUser(user);

        // lancement de l'activité Informations
        Intent informations = new Intent(this, InformationsActivity.class);
        informations.putExtra("filename", filename);
        startActivity(informations);
    }


    protected static class Utilisation implements LifecycleObserver {
        protected static int COMPTEUR = 0;

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        protected void NombreUtilisation() {
            COMPTEUR++;
        }
    }


    public void planning(View v) {
        // lancement de l'activité Planning
        Intent planning = new Intent(this, PlanningActivity.class);
        planning.putExtra("userId", id);
        startActivity(planning);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // cleanup de la database
        UserDao userDao = database.UserDao();
        userDao.deleteUsers(userDao.getAll());
        PlanningDao planningDao = database.PlanningDao();
        planningDao.deletePlannings(planningDao.getAll());
    }
}
